/*
 * Copyright (c) 2017 Baidu, Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.baidu.apm.monitor;

import com.baidu.apm.certificate.Certificate;
import com.baidu.apm.config.Config;
import com.baidu.apm.model.Request;
import com.baidu.apm.model.Response;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.concurrent.FutureCallback;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.nio.client.CloseableHttpAsyncClient;
import org.apache.http.impl.nio.client.HttpAsyncClients;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.SignatureException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

public class BotMonitor {

    // DuerOS rrequest for Bot. instance of Request
    private Request request;
    // The result of Bot returning to DuerOS. instance of Response
    private Response response;
    // request start time
    private Long requestStartTime;
    // request end time
    private Long requestEndTime;
    // event(intent handler) start time
    private Long eventStartTime;
    // event(intent handler) execution time
    private Long eventCostTime;
    // device event start time
    private Long deviceEveStartTime;
    // device event execution time
    private Long deviceEveCostTime;
    // custom events list
    private Map<String, Long> userEventList;
    // list of whether the custom events are called in pairs
    private Map<String, Boolean> isEventMakePair;
    // open application info,application name,or packagename,or deeplink
    private Map<String, String> appInfo;
    // the open audio url,like music url.etc
    private String audioUrl;
    // you private key
    private String privateKey;
    // your bot current status.0 represent debug, 1 represent online.
    private int environment;
    // is bot monitor enabled
    private boolean enabled;

    /**
     * BotMonitor constructor
     *
     * @param request request string
     */
    public BotMonitor(String request) {
        if (request == null || request.isEmpty()) {
            return;
        }
        this.requestStartTime = this.getMillisecond();
        this.eventCostTime = Long.valueOf(0);
        this.deviceEveCostTime = Long.valueOf(0);
        this.enabled = true;

        this.userEventList = new HashMap<>();
        this.isEventMakePair = new HashMap<>();
        this.appInfo = new HashMap<>();

        this.appInfo.put("appName", "");
        this.appInfo.put("packageName", "");
        this.appInfo.put("deepLink", "");

        this.request = new Request(request);
    }

    /**
     * set basic bot environment info
     *
     * @param privateKey rsa private key content
     * @param environment 0 represent debug, 1 represent online.
     */
    public void setEnvironmentInfo(String privateKey, int environment) {
        if (privateKey == null || privateKey.isEmpty()) {
            return;
        }
        this.environment = environment;
        this.privateKey = privateKey;
    }

    /**
     *
     * @param enabled is bot monitor sdk enable
     */
    public void setMonitorEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * @param responseData response with json format
     **/
    public void setResponse(String responseData) {
        if (responseData == null || responseData.isEmpty() || this.isShouldDisable()) {
            return;
        }

        this.requestEndTime = this.getMillisecond();
        this.response = new Response(responseData);
    }


    /**
     * when intent handler start,record current timestamp
     **/
    public void setEventStart() {
        this.eventStartTime = this.getMillisecond();
    }

    /**
     * when intent handler ends,calculate current event execution time
     **/
    public void setEventEnd() {
        this.eventCostTime = this.getMillisecond() - this.eventStartTime;
    }

    /**
     * define your own performance events,and call this funcion at the beginning of the event
     * Notice this function call must appear in pairs with setOprationToc
     *
     * @param taskName a name used to uniquely identify as user event
     **/
    public void setOprationTic(String taskName) {
        if (taskName == null || taskName.isEmpty() || this.isShouldDisable()) {
            return;
        }
        Long currTime = this.getMillisecond();
        this.userEventList.put(taskName, currTime);
        this.isEventMakePair.put(taskName, false);
    }

    /**
     * stop timing for an event,call this funcion at the end of the event
     * Notice this function call must appear in pairs with setOprationTic
     *
     * @param taskName a name used to uniquely identify as user event
     **/
    public void setOprationToc(String taskName) {
        if (taskName == null || taskName.isEmpty() || this.isShouldDisable()) {
            return;
        }
        Long oldTime = this.userEventList.get(taskName);
        Long currTime = this.getMillisecond();
        Long costTime = Long.valueOf(0);
        if (oldTime != null) {
            costTime = currTime - oldTime;
        }
        this.userEventList.put(taskName, costTime);
        this.isEventMakePair.put(taskName, true);
    }

    /**
     * set open application name
     *
     * @param appName application name
     **/
    public void setAppName(String appName) {
        if (appName == null || appName.isEmpty() || this.isShouldDisable()) {
            return;
        }
        this.appInfo.put("appName", appName);
    }

    /**
     * set package name
     *
     * @param packageName packageName
     **/
    public void setPackageName(String packageName) {
        if (packageName == null || packageName.isEmpty() || this.isShouldDisable()) {
            return;
        }
        this.appInfo.put("packageName", packageName);
    }

    /**
     * set deepLink
     *
     * @param deepLink the url to open app
     **/
    public void setDeepLink(String deepLink) {
        if (deepLink == null || deepLink.isEmpty() || this.isShouldDisable()) {
            return;
        }
        this.appInfo.put("deepLink", deepLink);
    }

    /**
     * set audio url
     *
     * @param audioUrl audio url
     **/
    public void setAudioUrl(String audioUrl) {
        if (audioUrl == null || audioUrl.isEmpty() || this.isShouldDisable()) {
            return;
        }
        this.audioUrl = audioUrl;
    }

    /**
     * get the current timestamp
     *
     * @return timestamp
     **/
    private Long getMillisecond() {
        return System.currentTimeMillis();
    }


    /**
     * upload data to server
     */
    public void uploadData() {
        if (this.isShouldDisable()) {
            return;
        }
        String botId = this.request.getBotId();
        String requestId = this.request.getRequestId();
        String query = this.request.getQuery();
        String reason = this.request.getReason();
        String deviceId = this.request.getDeviceId();
        String requestType = this.request.getRequestType();
        String userId = this.request.getUserId();
        String intentName = this.request.getIntentNameByIndex(0);
        String sessionId = this.request.getSessionId();
        Map<Object, Object> location = new HashMap<>();
        String slotName = this.response.getSlotName();
        Boolean shouldEndSession = this.response.getShouldEndSession();
        Object outputSpeech = this.response.getOutputSpeech();
        Object reprompt = this.response.getReprompt();

        Map<Object, Object> sysEvent = new HashMap<>();
        sysEvent.put("preEventList", new HashMap<>());
        sysEvent.put("postEventList", new HashMap<>());
        sysEvent.put("eventCostTime", this.eventCostTime);
        sysEvent.put("deviceEventCostTime", this.deviceEveCostTime);

        Long timestamp = Long.valueOf((System.currentTimeMillis() / 1000));
        Map<String, Object> dataContent = new HashMap<>();
        dataContent.put("sdkType", "java");
        dataContent.put("sdkVersion", Config.getSdkVersion());
        dataContent.put("requestId", requestId);
        dataContent.put("query", query);
        dataContent.put("reason", reason);
        dataContent.put("deviceId", deviceId);
        dataContent.put("requestType", requestType);
        dataContent.put("userId", userId);
        dataContent.put("intentName", intentName);
        dataContent.put("sessionId", sessionId);
        dataContent.put("location", location);
        dataContent.put("slotToElicit", slotName);
        dataContent.put("shouldEndSession", shouldEndSession);
        dataContent.put("outputSpeech", outputSpeech);
        dataContent.put("reprompt", reprompt);
        dataContent.put("audioUrl", this.audioUrl);
        dataContent.put("appInfo", this.appInfo);
        dataContent.put("requestStartTime", this.requestStartTime);
        dataContent.put("requestEndTime", this.requestEndTime);
        dataContent.put("timestamp", timestamp);

        dataContent.put("sysEvent", sysEvent);
        dataContent.put("usrEvent", this.userEventList);

        Map<String, Object> retData = new HashMap<>();
        retData.put("serviceData", dataContent);

        JSONObject jsonData = new JSONObject(retData);
        String jsonStr = jsonData.toString();
        String base64Str = Base64.getEncoder().encodeToString(jsonStr.getBytes());

        String pkversion;
        if (this.environment == 0) {
            pkversion = "debug";
        } else {
            pkversion = "online";
        }

        String signRet = "";
        try {
            signRet = Certificate.rsaSign(base64Str + botId + timestamp.toString() + pkversion,
                    this.privateKey, "utf-8");
        } catch (SignatureException e) {
            e.printStackTrace();
            return;
        }

        CloseableHttpAsyncClient httpClient = HttpAsyncClients.createDefault();
        httpClient.start();
        HttpPost httpPost = new HttpPost(Config.getUploadUrl());

        try {
            Map<String, String> contentMap = new HashMap<>();
            contentMap.put("data", base64Str);
            JSONObject jsonContent = new JSONObject(contentMap);
            httpPost.setEntity(new StringEntity(base64Str.toString()));
            httpPost.setHeader("SIGNATURE", signRet);
            httpPost.setHeader("botId", botId);
            httpPost.setHeader("timestamp", timestamp.toString());
            httpPost.setHeader("pkversion", pkversion);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        try {
            httpClient.execute(httpPost, new FutureCallback<HttpResponse>() {
                @Override
                public void completed(HttpResponse result) {
                }

                @Override
                public void failed(Exception ex) {
                }

                @Override
                public void cancelled() {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * is should disable botmonitor
     * @return boolean
     */
    public boolean isShouldDisable() {
        if (this.privateKey == null || this.privateKey.isEmpty()
                || this.request == null || this.response == null
                || !this.enabled)  {
            return true;
        }
        return false;
    }
}