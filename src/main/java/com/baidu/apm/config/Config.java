/*
 * Copyright (c) 2017 Baidu, Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.baidu.apm.config;

/**
 * BotMonitor config info
 *
 * @author liudesheng(liudesheng01@baidu.com)
 * @version V1.0.0
 * @since 2017.10.5
 */
public class Config {

    // data upload url
    private static String uploadUrl = "https://dueros-api.baidu.com/uploadmonitordata";
    // sdk version
    private static String sdkVersion = "1.0.0";

    public  static  String getUploadUrl() {
        return uploadUrl;
    }

    public  static  String getSdkVersion() {
        return sdkVersion;
    }
}
